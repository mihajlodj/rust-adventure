fn transpose(matrix: [[i32; 3]; 3]) -> [[i32; 3]; 3] {
    let mut row = 0;
    let mut column = 0;

    let mut new_matrix = [[0; 3]; 3];
    while row != 3 && column != 3 {
        let a = matrix[row][column];
        let b = matrix[column][row];
        
        new_matrix[row][column] = b;
        new_matrix[column][row] = a;
        
        row += 1;
        if row == 3 {
            column += 1;
            
            row = if column == 3 { row } else { 0 }
        }
        
    }
    return new_matrix;
}

fn pretty_print(matrix: &[[i32; 3]; 3]) {
    let mut row = 0;
    while row < 3 {
        let i = matrix[row][0];
        let j = matrix[row][1];
        let k = matrix[row][2];
        println!("[{i} {j} {k}]");
        row += 1;
    }
}

fn main() {
    let matrix = [
        [101, 102, 103],
        [201, 202, 203],
        [301, 302, 303],
    ];

    println!("matrix:");
    pretty_print(&matrix);

    let transposed = transpose(matrix);
    println!("transposed:");
    pretty_print(&transposed);
}