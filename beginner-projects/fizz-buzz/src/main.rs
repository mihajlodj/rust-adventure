fn main() {
    for number in 1..101 {
        if number % 15 == 0 {
            println!("fizzbuzz");
        } else if number % 3 == 0 {
            println!("fizz");
        } else if number % 5 == 0 {
            println!("buzz");
        } else {
            println!("{number}")
        }
    }
}
