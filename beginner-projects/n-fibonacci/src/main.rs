use std::io;
use std::io::Write;

fn fibonacci(n: u64) -> u64 {
    if n <= 1 {
        return 1;
    }
    return fibonacci(n - 1) + fibonacci(n - 2);
}


fn main() {
    print!("Please input a number: ");
    io::stdout().flush().unwrap(); // this causes panic in case of an error and program stops with its execution
    // io::stdout().flush().expect("Failed to flush stdout"); // this handles the error and program continues with execution

    let mut number = String::new();

    io::stdin()
        .read_line(&mut number)
        .expect("Failed to read line");

    let number: u64 = match number.trim().parse() {
        Ok(num) => num,
        Err(_) => 
            {
                println!("Invalid input.");
                return;
            },
    };
    println!("{}", fibonacci(number));

}
