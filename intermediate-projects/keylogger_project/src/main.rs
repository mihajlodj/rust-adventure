#[macro_use]
extern crate lazy_static;

use device_query::{DeviceEvents, DeviceState, Keycode};
use std::time::UNIX_EPOCH;
use std::{error::Error, env, fs::File, thread, time::Duration, time::SystemTime};
use std::sync::{Arc, Mutex};
use csv::Writer;

enum MouseKeys {
    LCLICK,
    WHEELCLICK,
    RCLICK
}

impl MouseKeys {
    fn to_string(&self) -> &str {
        match self {
            MouseKeys::LCLICK => "LCLICK",
            MouseKeys::WHEELCLICK => "WHEELCLICK",
            MouseKeys::RCLICK => "RCLICK",
        }
    }
    fn btn_to_string(button: usize) -> String {
        match button {
            1 => MouseKeys::LCLICK.to_string().to_string(),
            2 => MouseKeys::WHEELCLICK.to_string().to_string(),
            3 => MouseKeys::RCLICK.to_string().to_string(),
            _ => "UNKNOWN".to_string()
        }
    }
}

lazy_static! {
    static ref DATA: Arc<Mutex<Vec<Vec<String>>>> = Arc::new(Mutex::new(Vec::new()));
}

fn capture_clicks() {
    let device_state: DeviceState = DeviceState::new();
    let _mouse_guard = device_state.on_mouse_down({
        let clone = Arc::clone(&DATA);
        move |button: &usize| {
            let mut data: std::sync::MutexGuard<'_, Vec<Vec<String>>> = clone.lock().unwrap();
            let dur: Duration = SystemTime::now().duration_since(UNIX_EPOCH).expect("System time error at 'fn capture_clicks()'\n");
            data.push(vec![MouseKeys::btn_to_string(*button), dur.as_millis().to_string()]);
            println!("Added mouse click");   
        }});
    loop {
        
    }
}

fn capture_keys() {
    let device_state: DeviceState = DeviceState::new();
    let _guard = device_state.on_key_down({
        let clone = Arc::clone(&DATA);
        move |key: &Keycode| {
            let mut data: std::sync::MutexGuard<'_, Vec<Vec<String>>>  = clone.lock().unwrap();
            let dur: Duration = SystemTime::now().duration_since(UNIX_EPOCH).expect("System time error at 'fn capture_keys()'\n");
            data.push(vec![key.to_string(), dur.as_secs().to_string()]);
            println!("Added keystroke");
        }});
    loop {}
}

fn log_keys(file_path: String) -> Result<(), Box<dyn Error>> {
    // Create or open a file named 'log.csv' for writing
    let mut writer: Writer<File> = Writer::from_path(file_path)?;

    // Write the headers to the CSV file
    writer.write_record(&["Key", "Timestamp"])?;

    let mut guard: std::sync::MutexGuard<'_, Vec<Vec<String>>> = DATA.lock().unwrap();
    let data: &mut Vec<Vec<String>> = &mut *guard;
    // Write data to the CSV file
    for record in data {
        writer.write_record(record)?;
        
    }
    writer.flush()?;
    println!("Wrote to file");
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    
    if let Ok(home_dir) = env::var("HOME") {
        let file_path: String = format!("{}/log.csv", home_dir);

        let writing_handle: thread::JoinHandle<_> = thread::spawn(move || {
            let sleep_duration: u64 = 5;
            loop {
                thread::sleep(Duration::from_secs(sleep_duration));
                
                if let Err(_) = log_keys(file_path.clone()) {
                    panic!("Error while writing\n");
                }
            }
        });

        let keyboard_handle: thread::JoinHandle<()> = thread::spawn(|| {
            capture_keys();
        });
        let mouse_handle: thread::JoinHandle<()> = thread::spawn(|| {
            capture_clicks();
        });
        writing_handle.join().unwrap();
        keyboard_handle.join().unwrap();
        mouse_handle.join().unwrap();
        Ok(())
    } else {
        println!("HOME environment variable is not defined.");
        Ok(())
    }
    
}
