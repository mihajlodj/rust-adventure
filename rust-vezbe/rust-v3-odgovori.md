## Odgovori na pitanja iz trećih vežbi

1. Ownership u rustu je mehanizam koji sluzi da zameni garbage collector, a da pritom ne tera programere da brinu o pozivima za zauzimanje i oslobadjanje memorije. (Middle ground izmedju C-a i Jave) 

2. -Svaka vrednost ima svog 'vlasnika' (owener-a)  <br/>
-Moze imati samo jednog owenr-a u isto vreme  <br/>
-Kad owner izadje iz scope-a, vrednost se brise  <br/>

3. Scope se definise zagradama {}. U sustini blok koda u kojem su vidljive samo varijable definisane pre njega i u njemu (isto ko i u svakom drugom jeziku) 

4. string literal je nemutabilan po defaultu  

5. String varijabla je mutabilna po defaultu  

6. Varijabla se brise onda kada se poslednji put iskoristi u scope-u. Setite se zivotnog ciklusa iz spp-a, onih interference grafova i sranja. Isti djavo.  

7. Drop se koristi da obrises vrednost pre nego sto izadje iz scope-a. U sustini, izvrsava se automatski na kraju scope-a za svaku varijablu definisanu u njemu.  

8. Za tip String, pointer se cuva na stacku, a pointer pokazuje na niz karaktera koji se cuvaju na heapu. Takodje cuva i duzinu stringa i kapacitet bloka u heapu.  

9. Kada radi sa pointerima, shallow copy postaje "move", zbog pravila koje glasi da ne moze postojati vise od jednog pointera na jedan objekat u heapu. Ako nije u pitanju pointer, onda se jednostavno kopira vrednost u stacku (ovo se u sustini desava samo sa primitivnim tipovima: int, char, itd. Svi kompleksni tipovi se premestaju, 'move-uju'). A deep copy radi kako smo navikli.  

10. `let s2 = s1` je shallow copy, tj move. Deep copy se eksplicitno poziva sa .clone()  

11. Premestanje vlasnistva, ownership-a, je ovo sto sam napisao u 9. Ali ima jos nacina na koje se vlasnistvo moze premestiti (ili oduzeti)  

12. Kod daje error jer ovde 's1' vise ne postoji nakon `let s2 = s1;`  

13. Copy trait oznacava da se tip kopira po bitima, a ne radi se move. Ovo sto sam naveo u 9. odgovoru su tipovi koji implementiraju Copy trait. Zbog toga se oni i dalje mogu koristiti nakon move-a.  

14. Primeri pored int i char su boolean, i ostali integer i float tipovi, kao u32 i f64  

15. Ne, reference nemaju vlasnistvo. One nam sluze da koristimo objekat bez da imamo nad njime.  

16. Koncept pozajmljivanja 'borrowing', bukv ovo sto sam pomenuo u 15. odgovoru. Kada pozajmimo objekat, mozemo da ga koristimo, a da nismo owner-i. Primer ovoga je `println!` macro. Posle svakog njegovog poziva, mi i dalje mozemo koristiti varijable koje smo mu dali.  

17. Ne, reference/pointeri nisu mutabilne po defaultu.  

18. Reference mogu biti mutabilne ako iskoristimo `mut` keyword.  

19. Kao sto sam pre pomenuo, objekat ne moze imati vise od jedne reference na sebe. U suprotnom moze doci do trke do podataka (data race). To je pravilo koje je uvedeno da bi se izbegao taj problem u vremenu kompajliranja (a ne u runtime-u, kako moze biti u drugim jezicima).  

20. 3 uslova pod kojima moze doci do trke do podataka:
-Kada vise thread-ova dele iste podatke
-Kada konkurentni procesi dele iste podatke
-Kada barem jedan od konkurentnih procesa ima write access nad objektom

21. Ne. Jedan objekat ne moze imati mutabilnu i nemutabilnu referencu u isto vreme. Pravilo glasi da mozes imati ili proizvoljan broj nemutabilnih referenci, ili samo jednu mutabilnu.

22. Snippet je validan zato sto, iako naizgled imamo 2 nemutabilne i mutabilnu referencu, 2 nemutabilne reference zavrsavaju svoj zivotni ciklus posle println! macro poziva, jer su tu poslednji put iskoriscene u scope-u. Tricky tricky pitanje.

23. Scope (zivotni ciklus) reference je u sustini mesto u kodu u kojem se ona moze koristiti.

24. Dangling pointer (viseca referenca) se stvara kada zelimo da iz funkcije vratimo referencu na objekat koji je bio kreiran u njoj. Ovo je ilegalno zato sto se vrednost dropuje kada se funkcija zavrsi, tako da bi vraceni pointer ukazivao na nevalidnu memoriju.

25. Ispravljen kod, viola.
```rust
fn main() {
  let reference_to_nothing = dangle();
}

fn dangle() -> String {
    let s = String::from("hello");

    s
}
```

26. <i>At any given time, you can have either one mutable reference or any number of immutable references.</i> True

27. Slice tip nam omogucava da iz niza podataka uzmemo neki kontinualni podskup. Bukv ko i u svakom drugom jeziku.

28. [start_idx..end_idx], [start_idx..], [..end_idx], [..]

29. Kod puca u 18oj liniji, na s.clear(); .clear() trazi mutabilnu referencu dok je nasa nemutabilna referenca (let word) jos uvek 'ziva'. Rip.

30. string literali ( let s: &str = "text"; ) su slice, yes. Ali nisu mutabilni. &str su nemutabilne reference

31. Tip slice-a je &[i32]

32. Kod samo koristi nemutabilne reference zato se uspesno kompajlira.

33. U kodu menjamo samo 'takes_ownership' definiciju, i poziv. Zato sto je String tip koji ce izgubiti vlasnistvo kada se pozove funkcija (bez &). Medjutim, x je literal int, i zato nece praviti problem.
```rust
fn main() {
    let s = String::from("hello");  // s comes into scope
  
    takes_ownership(&s);             
    println!("{}", s);
  
    let x = 5;                      
  
    makes_copy(x);   
    
    println!("{}", x)
  } 
  
  fn takes_ownership(some_string: &String) {
      println!("{}", some_string);
  } 
  fn makes_copy(some_integer: i32) {
      println!("{}", some_integer);
  }
```

34. Kod se kompajlira bez gresaka, jer je sasvim legalno shufflovati vlasnistvo na taj nacin.