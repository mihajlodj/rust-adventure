1. 
```rust
    fn largest_i32<T>(list: &[T]) -> &T {
    let mut largest = &list[0];

    for item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn main() {
    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest_i32(&number_list);
    println!("The largest number is {}", result);
    assert_eq!(*result, 100);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest_char(&char_list);
    println!("The largest char is {}", result);
    assert_eq!(*result, 'y');
}
```

2. 
```rust
struct Point<T, Y> {
    x: T,
    y: Y,
}

fn main() {
    let wont_work = Point { x: 5, y: 4.0 };
}
```

3. Da.

4. `p3.x = 5, p3.y = c`

5. Ne, zbog mehanizma koji se zove 'monomorfizam'. Rust kompajler trazi pozive svih funkcija koje koriste generike, i na osnovu prosledjenih parametara, pravi kopije funkcija sa odgovarajucim potpisom.

6. Trait. Trait je kao interfejs u javi, osim sto moze imati defaultnu implementaciju.

7. Ne, jer ne mozes implementirati traitove na eksterne tipove (tipove van crate-a). Ovo se zove <i>coherence</i>

8. Ne. Defaultna implementacija se moze pozvati samo ako ne overridujes metod.

9. `New article available! Penguins win the Stanley Cup Championship!, by Iceburgh (Pittsburgh, PA, USA)
1 new tweet: (Read more from @horse_ebooks...)`

10. `pub fn notify<T: Summary>(item1: &T, item2: &T)`

11. 
```rust
pub fn notify<T: Summary + Display>(item: &T) {}
```

12. 
```rust 
fn some_function<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32 {}

fn some_function<T, U> (t: &T, u: &U) -> i32 where T: Display + Clone, U Clone + Debug {}
```

13. Da, zato sto poslednja funkcija moze vratiti dva razlicita tipa. Ovo rustc ne dozvoljava.

14. `Blanket implementation` Je kada implementiramo trait nad generic-om koji implementira neki drugi trait.
Ako imamo trait `A`, generic `T` i trait `B`: generic `T` je tipa `B` (<T: B>). Kada implementiramo trait `A` za generic `T`, `impl <T: B> A for T`, onda ce kompajler, za svaki tip `T` koji implementira `B`, automatski implementirati i `A` u compile time-u.

15. `Reference lifetime` je zivotni vek reference. Po defaultu traje do kod ne izadje iz scope-a.

16. Da, produkuje error, jer kompajler ne daje da koristimo null vrednost. Evo fix-a:
```rust
fn main() {
    let r: i32 = 5;
    println!("r: {}", r);
}
```

17. Da. Produkuje error jer ce na kraju scope-a, program osloboditi memoriju x-a, pa ce pointer pokazivati na memoriju koja vise ne pripada programu.

18. `Borrow checker` proverava duzinu zivotnog veka referenci da bi osigurao da se referenca ne koristi nakon sto joj je zivotni vek istekao.

19. 
```rust
fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

20. Validan je, zato sto se referenca sa kracim zivotnim vekom poslednji put koristi unutar svog scope-a. Pritom je funkcija longest osigurala da ce vratiti referencu sa kracim zivotnim vekom.

21. Da, zato sto kompajler ne zna koju ce referencu program vratiti u runtime-u, a posto smo eksplicitno naveli da return vrednost funkcije ima zivotni vek jednak kracem veku dva parametra, on sprecava kompajliranje. Kljuc je sto je taj kraci zivotni vek dug kolko i taj kraci scope u kojem se drugi parametar funkcije definise.

22. 
```rust
fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);
}

fn longest<'a>(x: &'a str, y: &str) -> &'a str {
    let result = String::from("really long string");
    result.as_str()
}
```

23. Trebamo vratiti 'owned datatype', tj. String umesto reference.
```rust
fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);
}

fn longest<'a>(x: &str, y: &str) -> String {
    let result = String::from("really long string");
    result;
}
```

24. Input i output lifetimes su zivotni vekovi input parametara i output tipova.

25. To su pravila koje rustc koristi kad proba da resi zivotne vekove referenci u compile time-u. Ako posle sva 3 pravila ne uspe da resi sve reference, baca se error.

26. Ne moramo, jer ce posle prva dva pravila kompajler resiti sve reference.

27. 
```rust
struct ImportantExcerpt<'a> {
    part: &'a str,
}

impl<'a> ImportantExcerpt<'a> {
    fn level(&self) -> i32 {
        3
    }
}

impl<'a> ImportantExcerpt<'a> {
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Attention please: {}", announcement);
        self.part
    }
}

fn main() {
    let novel = String::from("Call me Ishmael. Some years ago...");
    let first_sentence = novel.split('.').next().expect("Could not find a '.'");
    let i = ImportantExcerpt {
        part: first_sentence,
    };
}
```

28. `let s: &'static str = "I have a static lifetime.";`

29. string literal zivi cele duzine programa.

30. The what?

31. Ne panici.

..... Dalja pitanja pojma nemam
