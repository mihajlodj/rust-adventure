## Odgovori na pitanja iz četvrtih vežbi

1. 
```rust
struct user { 
	active: true,
	username,
	email,
	sign_in_count: 1,
}
```

2. 
```rust
let user2 = User {
	email: String::from("another@example.com"),
	..user1
}
```
3. Ne.
4. Da. Strukture bez polja su validne u rust-u.
5. 
```rust
#[derive(debug)]
struct Rectangle { height: u32, width: u32 }
fn area(rect: &Rectangle) -> u32 { rect.height * rect.width }
```
6. 
```rust
impl Rectangle { fn area(&self) -> u32 { self.height * self.width } }
```
7. Struct samo ja msm.
8. Self je alias tip koji predstavlja ime naseg structa. Kad kazemo samo '&self', to je skracenica za 'self: &Self'. 
9. Da, moze. Nije cesto potrebno.
10. Kada hocemo da oduzmemo vlasnistvo da bismo izvrsili neku transformaciju, posle koje ne zelimo da se taj isti objekat dalje koristi.
11. 
```rust
impl Rectangle { 
	fn square(side: u32) -> Self { 
		let self { width: side, height side }
	} 
}
```
12. Da. Moze.
13. Tipovi koji sluze da numerisu.
14. 
```rust
enum IpAddr {
	V4(String),
	V6(String)
}
```
15. Mislim da ovo ne pravi error? Reko bih da ne pravi.
16. Rust nema koncept 'null' vrednosti, ali ima Option<T>, koji je Enum. On sluzi da osigura da ne moramo pretpostavljati da li vrednost postoji u promenljivoj ili ne. U sustini, zaobilaznica za null pointer exception.
17. 
```rust
fn main() {
	let _absent_number: Option<i8> = None;
}
```
Ovo je samo jedan nacin, poenta da se kompajleru eksplicitno kaze kog je tipa, jer ne moze sam da pretpostavi.
18. Option<T> nam eksplicitno govori da li je vrednost prisutna ili nije, tako da ce se za cuveni 'NullPointerException' znati vec u compile-time-u, umesto samo u runtime-u.
19. Match arms - su rust-ova verzija Switch-case-a. Svake arm je svoj case.
20. Bilo koja vrednost koju mi definisemo.
21. "State quarter is from Alaska!"
22. 
```rust
five: Some(
    5,
), six: Some(
    6,
), none: None
```
23. Match nije pokrio sve moguce slucajeve, pa kompajler baca error.
24. 
```rust
fn main() {
  let dice_roll = 9;
  match dice_roll {
      3 => add_fancy_hat(),
      7 => remove_fancy_hat(),
      _ => ()
  }

  fn add_fancy_hat() {}
  fn remove_fancy_hat() {}
}
```
25. 
```rust
let config_max = Some(3u8);
if let Some(max) = config_max {
	println!("The maximum is configured to be {}", max);
}
```
26. Match je robustniji, if let je citljiviji i koncizniji.
27. 
```rust
#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
    // --snip--
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn main() {
    let coin = Coin::Penny;
    let mut count = 0;
    if let Coin::Quarter(State) = coin {
	println!("State quarter from {:?}!", state);
    } else {
	count += 1;
    }
    println!("The count is: {}", count)
}
```














