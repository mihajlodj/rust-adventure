## Odgovori na pitanja iz petih vežbi

1. Koji su elementi module sistema? -> Packages, Crates, Modules, Paths  <br/>

2. Crate je osnovna jedinica module sistema. To je prva stvar koju kompajler pregleda i prevodi.  <br/>

3. Binary crate je executable koji se stvori kada rust kompajlira kod. Da bi se smatrao binary crate-om, kod mora imati main() funkciju.  <br/>

4. Library crate je isto sto i binary, samo sto nema main() funkciju. Tj, on sluzi da bi mogo da se importuje na drugim mestima i da se koriste njegove funkcionalnosti.  <br/>

5. Crate root je source fajl od kojeg rust kompajler pocinje da prevodi kod.

6. Package je skup jednog ili vise crate-a. Poseduje Cargo.toml fajl u kojem je navedeno na koji nacin se builduju (kompajliraju) ti crate-ovi.  <br/>

7. Package moze imati najvise 1 library crate i koliko god hocemo da ima binary crate-a.  <br/>

8. src/main.rs je crate root binary crate-a, a src/lib.rs je crate root library crate-a.  <br/>

9. Binary crate-ovi mogu stojati i u src/bin folder.  <br/>

10. Konvencije koje trebamo da pratimo kada definisemo module su:
ne znam. Ali kontam da ih imenujemo na isti nacin kao pakete.

11. Moduli su privatni po defaultu, da.

12. path::to::module. Takodje postoji `super`, ako hocemo da idemo korak unazad.

13. Po defaultu, roditeljski modul nema pristup sadrzaju unutrasnjeg modula.

14. Absolutni path: crate::front_of_house::hosting::add_to_waitlist();  Relativni path: front_of_house::hosting::add_to_waitlist();

15. Ovo pitanje me buni, jer mislim da ne moze. Kod se takodje ne kompajlira, bas zato sto je `hosting` modul i `add_to_waitlist()` funkcija privatna.

16. `Super` se koristi kao ../ prilikom pisanja putanja.

17. `seasonal_fruit` se treba proglasiti public

18. Jer je po defaultu privatna, lol

19. Kada je enum public, sva njegova polja su takodje public. To je karakteristicno samo za njih. Struct polja moramo pojedinacno da proglasimo public, kao i metode.

20. Produkuje error zato sto je `use` iskorisceno van scope-a customer modula. Sto je zbunjujuce sobzirom da dete-modul moze da vidi vrednosti iz roditelja. Ali se to ne odnosi na `use` putanje. Ovo se moze ispraviti tako sto se stavi `pub use`.

21. Prvi nacin da se popravi je naravno da se koristi apsolutna ili relativna putanja. Drugi nacin je da se `use` pomeri u scope `mod customer`-a.

22. Re-exporting names je samo lepljenje `pub` na `use`, tako da ga i deca-moduli mogu koristiti. (U sustini, to je treci nacin da se ispravi kod iz 20og zadatka)

23. Koraci za importovanje stranih modula su:

Dodati ime modula u cargo.toml fajl, pa onda `use` nad bibliotekom. Kao i u svakom drugom jeziku, lol.

24. Ime standardne biblioteke je std, kao i u c-u

25. 
```rust
use std::{cmp::Ordering, io};
```

26. 
```rust
use std::io::{self, Write};
```

27. Arrays se cuvaju na stacky. Tuples se cuvaju na stacku (valjda).

28. Vektori i hash mape se cuvaju na heapu.

29. U sustini, vektorima moras precizirati koju vrednost cuvaju.
```rust
fn main() {
	let v: Vec<String> = Vec::new();
}
```

30. Vektor mora biti mutabilan da bi mu dodavao vrednost
```rust
fn main() {
	let mut v = Vec::new();
	
	v.push(5);
	v.push(6);
	v.push(7);
	v.push(8);
	}
```

31. Output je panic zato sto je indeks preveliki. Evo alternative:
```rust
fn main() {
	let v: Vec<u8> = (1..102).collect();
	
	let does_not_exist = &v[100];
} // ili
fn main() {
	let v = vec![1, 2, 3, 4, 5];
	
	let does_exist = &v[4];
}
```

32. Kod ne radi jer .push() moze izazvati da se memorija oslobodi, da bi se alocirao novi niz, u koji moze da stane novi element koji se pushuje.
U tom slucaju bi pointer ostao da pokazuje na memoriju koja vise nije alocirana (dangling pointer). Kompajler nas sprecava da to radimo.

33. Borrowing pravilo glasi da mi ne smemo menjati vrednost promenljive koju pozajmljujemo. Kada pozovemo push(), moze se desiti da menjamo vrednost koju smo pozajmili (kao sto sam objasnio u 32om pitanju), pa zato program puca.

34. Error se desava jer `for i in &v` daje immutable referencu. Takodje, zelimo da menjamo vrednost na koju ona pokazuje, a ne samu referencu. Dakle:
```rust
fn main() {
let mut v = vec![100, 32, 57];
for i in &mut v {
    *i += 50;
}
}
```

35. Baca error jer pravimo drugu mutabilnu referencu nad nizom. Rust compiler to ne dozvoljava. (Strozije: push() pravi mutabilni borrow, sto ne smemo da radimo jer vec borrow-ujemo mutabilnu referencu sa `for i in &mut v`)

36. Jeste.

37. Vector je dropovan kad se izadje iz scope-a.

38. str/&str je string slice koji je deo core rust-a, dok je String deo standardne biblioteke.

39. 
```rust
fn main() {
let s = "initial contents".to_string();
}
```

40. Ne.

41. `+` operator uzima vlasnistvo nad levim operandom, a od drugog trazi referencu. jer potpis otp izgelda ovako: `fn add(self, s: &str) -> String`
```rust
fn main() {
let s1 = String::from("Hello, ");
let s2 = String::from("world!");
let s3 = s1 + &s2; // note s1 has been moved here and can no longer be used
}
```

42. Proizvodi error jer je `+` operator iz prethodne linije preuzeo vlasnistvo nad `s1`, dakle vise ne mozemo da ga koristimo.

43. tic, tac, toe: tic-tac-toe

44. Produkuje error, jer String tip ne moze da se indeksira.

45. Зд

46. Da, iz istog razloga sto prethodni kod printuje samo dva slova. Cirilica (non-ascii) karakteri zauzimaju vise od 1 bajta.

47. Da. Prvi ce printovati karaktere, drugi brojne vrednosti.

48. copied() se poziva da bi se dobio `Option<u8>` umesto `Option<&u8>`. Unwarp_or se koristi da bi se vratila neka vrednost u slucaju da je Option dao None. U sustini, skracenica za match.

49. 
```rust
fn main() {
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

for (k, v) in &scores {
    // nesto
}
}
```

50. Error je u poslednjoj liniji. Hashmap insert uzima vlasnistvo nad varijablama, tako da se one vise ne mogu pozivati u kodu.

51. `{"Blue": 25}`

52. `{"Yellow": 50, "Blue": 10}`

53. Ako kljuc postoji u mapi, vrednost mu se ne menja. Ako ne postoje, insertuje se sa vrednoscu koja stoji u `.or_insert()` metodi. U pitanju je greska. Misli se na `.or_insert`, `.or_entry` ne postoji.

54. `{"world": 2, "wonderful": 1, "hello": 1}`

55. recoverable and unrecoverable errors

56. Pozivanjem `panic!` macro-a.

57. Unwinding je brisanje memorije koju je program koristio pre nego sto se uspanicio. Mozemo ovo da sprecimo tako sto dodamo `[profile.release] \n panic = 'abort'` u Cargo.toml

58. Panici. Mozemo da stavim environment varijablue: `RUST_BACKTRACE=1` i printace stacktrace kad pukne.

59. 
```rust
use std::fs::File;

File::open("hello.txt") match {
    case Ok(file) => println!("Worked");
    case Err => println!(":<")
}
```

60. The f*ck are closures? To je chapter 13, ne hvala.

61. 
```rust
use std::fs::File;

fn main() {
    let greeting_file = File::open("hello.txt").unwrap();
}
```

62. 
```rust
use std::fs::File;

fn main() {
    let greeting_file = File::open("hello.txt")
        .expect("hello.txt should be included in this project");
}
```

63. Expect je bolji za debagovanje jer imas poruke.

64. 
```rust

#![allow(unused)]
fn main() {
use std::fs::File;
use std::io::{self, Read};

fn read_username_from_file() -> Result<String, io::Error> {
    let mut username_file = File::open("hello.txt")?;
    let mut username = String::new();
    match username_file.read_to_string(&mut username)?;\
    Ok(username);
}
}
```

65. ? se ne moze koristiti u funkcijama koje ne vracaju Result (ili Option).

66. Main funkcija moze vracati i Result tip.

67. Mrzi me....